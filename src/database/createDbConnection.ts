import { DbConfig } from '../types'
import ApiDataSource from './ApiDataSource'

export default async function connectDb(dbConfig: DbConfig) {
    // overriding options to cover test cases
    ApiDataSource.setOptions({ port: dbConfig.port })
    ApiDataSource.setOptions({ host: dbConfig.host })
    ApiDataSource.setOptions({ username: dbConfig.username })
    ApiDataSource.setOptions({ password: dbConfig.password })

    try {
        await ApiDataSource.initialize()
        console.info('Data source initialized at host...', dbConfig.host)

        return ApiDataSource
    } catch (error) {
        console.error('Db connection error:', error)
    }
}
