import { Request, Response, Router } from 'express'
import MovieReviewController from './MovieReview.controller'
import { validate } from 'uuid'
import { isError as isJoiError } from 'joi'
import { isControllerError } from '../types'
import MovieController from '../movies/Movie.controller'
import { permissionsType } from '../constants'
import { requiredScopes } from 'express-oauth2-jwt-bearer'

const movieReviewRouter = Router({ mergeParams: true })

movieReviewRouter.get(
    '/',
    requiredScopes(permissionsType.movieReviewsRead),
    async (request: Request, response: Response) => {
        const { movieId } = request.params

        if (validate(movieId)) {
            const movie = await MovieController.getMovieById(movieId)
            if (movie) {
                const movieReviewList =
                    await MovieReviewController.getMovieReviewList()
                response.json(movieReviewList)
            } else {
                return response.status(404).json({
                    message: `Movie with id:${movieId} was not found.`
                })
            }
        } else {
            return response.status(400).json({
                message: `The id provided: ${movieId} is not a valid uuid version 4.`
            })
        }
    }
)

movieReviewRouter.get(
    '/:reviewId',
    requiredScopes(permissionsType.movieReviewsRead),
    async (request: Request, response: Response) => {
        const { movieId, reviewId } = request.params

        try {
            if (validate(movieId)) {
                const movie = await MovieController.getMovieById(movieId)
                if (movie) {
                    if (validate(reviewId)) {
                        const movieReview =
                            await MovieReviewController.getMovieReviewById(
                                reviewId
                            )
                        if (movieReview) {
                            return response.json(movieReview)
                        } else {
                            return response.status(404).json({
                                message: `Moview review with id: ${reviewId} does not exists.`
                            })
                        }
                    } else {
                        return response.status(400).json({
                            message: `The id provided for the review: ${reviewId} is not a valid uuid version 4.`
                        })
                    }
                } else {
                    return response.status(404).json({
                        message: `Movie with id:${movieId} was not found.`
                    })
                }
            } else {
                return response.status(400).json({
                    message: `The id provided for the movie: ${movieId} is not a valid uuid version 4.`
                })
            }
        } catch (error) {
            console.error(error)
            return response.status(500).json({
                message: `An internal error occurred.`
            })
        }
    }
)

movieReviewRouter.post(
    '/',
    requiredScopes(permissionsType.movieReviewsWrite),
    async (request, response) => {
        const { movieId } = request.params as { movieId: string }

        // validating the movie exists
        if (!validate(movieId)) {
            return response.status(400).json({
                message: `The id provided for the movie: ${movieId} is not a valid uuid version 4.`
            })
        } else {
            const movie = await MovieController.getMovieById(movieId)
            if (!movie) {
                return response.status(404).json({
                    message: `The id provided for the movie: ${movieId} is not a valid uuid version 4.`
                })
            }
        }

        // request input validation
        const newMovieReview = await MovieReviewController.createMovieReview(
            movieId,
            request.body
        )

        if (isJoiError(newMovieReview)) {
            response.status(400).json({
                message: newMovieReview.message
            })
        } else if (isControllerError(newMovieReview)) {
            response.status(500).json({
                message: newMovieReview
            })
        } else {
            response.status(200).json(newMovieReview)
        }
    }
)

movieReviewRouter.delete(
    '/:reviewId',
    requiredScopes(permissionsType.movieReviewsWrite),
    async (request, response) => {
        try {
            const { reviewId, movieId } = request.params as {
                movieId: string
                reviewId: string
            }

            if (!validate(reviewId)) {
                return response.status(400).json({
                    message: `The id provided: ${reviewId} is not a valid uuid version 4.`
                })
            }
            if (!validate(movieId)) {
                return response.status(400).json({
                    message: `The id provided: ${movieId} is not a valid uuid version 4.`
                })
            }

            const movie = await MovieController.getMovieById(movieId)

            if (movie) {
                const review = await MovieReviewController.getMovieReviewById(
                    reviewId
                )

                if (review) {
                    const result =
                        await MovieReviewController.deleteMovieReviewById(
                            reviewId
                        )
                    if (isControllerError(result)) {
                        console.error(result)
                        return response.status(500).json({
                            message: `An error ocurrent when deleting the movie review.`
                        })
                    } else {
                        return response.status(204).json()
                    }
                } else {
                    return response.status(404).json({
                        message: `Movie Review with id:${reviewId} was not found.`
                    })
                }
            } else {
                return response.status(404).json({
                    message: `Movie with id:${movieId} was not found.`
                })
            }
        } catch (error) {
            console.error(error)
            return response.status(500).json({
                message: `An internal error occurred.`
            })
        }
    }
)

movieReviewRouter.patch(
    '/:reviewId',
    requiredScopes(permissionsType.movieReviewsWrite),
    async (request, response) => {
        try {
            const { reviewId, movieId } = request.params as {
                reviewId: string
                movieId: string
            }

            if (!validate(reviewId)) {
                return response.status(400).json({
                    message: `The id provided: ${reviewId} is not a valid uuid version 4.`
                })
            }
            if (!validate(movieId)) {
                return response.status(400).json({
                    message: `The id provided: ${movieId} is not a valid uuid version 4.`
                })
            }

            const movie = await MovieController.getMovieById(movieId)

            if (!movie) {
                return response.status(404).json({
                    message: `Movie with id: ${movieId} does not exists.`
                })
            }

            const updateResult =
                await MovieReviewController.updateMovieReviewById(
                    reviewId,
                    request.body
                )

            if (isJoiError(updateResult)) {
                response.status(400).json({
                    message: updateResult.message
                })
            } else if (isControllerError(updateResult)) {
                if (
                    updateResult.error_id ===
                    'UPDATE_MOVIE_REVIEW_INTERNAL_ERROR'
                ) {
                    response.status(500).json({
                        message: updateResult.message
                    })
                } else if (
                    updateResult.error_id === 'UPDATE_MOVIE_REVIEW_NOT_FOUND'
                ) {
                    response.status(404).json({
                        message: `Movie Review with id: ${reviewId} not found.`
                    })
                }
            } else {
                response.status(200).json(updateResult)
            }
        } catch (error) {
            console.error(error)
            return response.status(500).json({
                message: `An internal error occurred.`
            })
        }
    }
)

export default movieReviewRouter
