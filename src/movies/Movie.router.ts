import { Request, response, Response, Router } from 'express'
import MovieController from './Movie.controller'
import { v4 as uuid, validate } from 'uuid'
import { MovieCreateInputSchema } from './Movie.validators'
import { isError as isJoiError } from 'joi'
import { isControllerError } from '../types'
import { permissionsType } from '../constants'
import { requiredScopes } from 'express-oauth2-jwt-bearer'

const movieRouter = Router()

movieRouter.get(
    '/',
    requiredScopes(permissionsType.moviesRead),
    async (request: Request, response: Response) => {
        const movieList = await MovieController.getMovieList()
        response.json(movieList)
    }
)

movieRouter.get(
    '/:id',
    requiredScopes(permissionsType.moviesRead),
    async (request: Request, response: Response) => {
        const id: string = request.params.id

        try {
            if (validate(id)) {
                const movie = await MovieController.getMovieById(id)
                if (movie) {
                    return response.json(movie)
                } else {
                    return response.status(404).json({
                        message: `Movie with id:${id} was not found.`
                    })
                }
            } else {
                return response.status(400).json({
                    message: `The id provided: ${id} is not a valid uuid version 4.`
                })
            }
        } catch (error) {
            console.error(error)
            return response.status(500).json({
                message: `An internal error occurred.`
            })
        }
    }
)

movieRouter.post(
    '/',
    requiredScopes(permissionsType.moviesWrite),
    async (request, response) => {
        // console.log('REST POST to create new movie!')

        // request input validation
        const newMovie = await MovieController.createMovie(request.body)

        if (isJoiError(newMovie)) {
            response.status(400).json({
                message: newMovie.message
            })
        } else if (isControllerError(newMovie)) {
            response.status(500).json({
                message: newMovie
            })
        } else {
            response.status(200).json(newMovie)
        }
    }
)

movieRouter.delete(
    '/:id',
    requiredScopes(permissionsType.moviesWrite),
    async (request, response) => {
        try {
            const id: string = request.params.id

            if (id && validate(id)) {
                const movie = await MovieController.getMovieById(id)
                if (movie) {
                    const result = await MovieController.deleteMovieById(id)
                    if (isControllerError(result)) {
                        console.error(result)
                        return response.status(500).json({
                            message: `An error ocurrent when deleting the movie.`
                        })
                    } else {
                        return response.status(204).json()
                    }
                } else {
                    return response.status(404).json({
                        message: `Movie with id:${id} was not found.`
                    })
                }
            } else {
                return response.status(400).json({
                    message: `The id provided: ${id} is not a valid uuid version 4.`
                })
            }
        } catch (error) {
            console.error(error)
            return response.status(500).json({
                message: `An internal error occurred.`
            })
        }
    }
)

movieRouter.patch(
    '/:id',
    requiredScopes(permissionsType.moviesWrite),
    async (request, response) => {
        try {
            const id = request.params.id

            if (validate(id)) {
                const updateResult = await MovieController.updateMovieById(
                    id,
                    request.body
                )

                if (isJoiError(updateResult)) {
                    response.status(400).json({
                        message: updateResult.message
                    })
                } else if (isControllerError(updateResult)) {
                    if (
                        updateResult.error_id === 'UPDATE_MOVIE_INTERNAL_ERROR'
                    ) {
                        response.status(500).json({
                            message: updateResult.message
                        })
                    } else if (
                        updateResult.error_id === 'UPDATE_MOVIE_NOT_FOUND'
                    ) {
                        response.status(404).json({
                            message: `Movie with id: ${id} not found.`
                        })
                    }
                } else {
                    response.status(200).json(updateResult)
                }
            } else {
                return response.status(400).json({
                    message: `The id provided: ${id} is not a valid uuid version 4.`
                })
            }
        } catch (error) {
            console.error(error)
            return response.status(500).json({
                message: `An internal error occurred.`
            })
        }
    }
)

export default movieRouter
