import 'isomorphic-fetch'

async function getAuthTokenTest(): Promise<string> {
    try {
        const authResponse = await fetch(
            `${process.env.AUTH_TENANT_URL}/oauth/token` || '',
            {
                method: 'POST',
                headers: {
                    ['Content-Type']: 'application/json'
                },
                body: JSON.stringify({
                    client_id: process.env.TEST_AUTH_CLIENT_ID,
                    client_secret: process.env.TEST_AUTH_CLIENT_SECRET,
                    audience: process.env.AUTH_AUDIENCE,
                    grant_type: 'client_credentials',
                    scope: 'read:movies write:movies read:movie-reviews write:movie-reviews'
                })
            }
        )

        const tokenData = await authResponse.json()

        console.log('AUTH TOKEN!!!', tokenData)

        return tokenData.access_token
    } catch (error) {
        console.error(error)
        return '' // this will be an invalid token
    }
}

export default getAuthTokenTest
