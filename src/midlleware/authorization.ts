import { Response, Request, NextFunction } from 'express'

const authorizationMiddleware = (routePermissionList: string[]) => {
    return function (request: Request, response: Response, next: NextFunction) {
        const userPermissions = request.auth?.payload?.permissions as string[]
        if (userPermissions && userPermissions.length > 0) {
            const isAllowed = routePermissionList.reduce(
                (isAllowed, routePerm) => {
                    if (
                        userPermissions.some(
                            (userPerm) => userPerm === routePerm
                        )
                    )
                        return true
                    return isAllowed
                },
                false
            )
            if (isAllowed) {
                next()
            } else {
                response.status(403).json({
                    message: "You don't have access to this resource"
                })
            }
        } else {
            response.status(403).json({
                message: "You don't have access to this resource"
            })
        }
    }
}

export default authorizationMiddleware
